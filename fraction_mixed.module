<?php
/**
 * @file
 *  Core code for the "Mixed fraction" module.
 *
 *  � 2014-2015 RedBottle Design, LLC and Five Star Tool Company.
 *  All rights reserved. This code is licensed under the GNU GPL v2 or later.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com)
 */

require_once 'fraction_mixed.constants.inc';
require_once 'fraction_mixed.field.inc';
require_once 'fraction_mixed.feeds.inc';

/***************************************************************
 * Utility functions
 ***************************************************************/
/**
 * MixedFraction factory function.
 *
 * Create a new mixed fraction from the provided numerator and denominator.
 *
 * @param $numerator
 *   The fraction's numerator.
 *
 * @param $denominator
 *   The fraction's denominator.
 *
 * @return  MixedFraction
 *   Returns a new mixed fraction object.
 */
function mixed_fraction($numerator = 0, $denominator = 1) {
  return new MixedFraction(NULL, $numerator, $denominator);
}

/**
 * MixedFraction factory function.
 *
 * Convert a mixed number string value into a MixedFraction object.
 *
 * The number should be in the format "X-Y/Z", where X is the whole number,
 * Y is the numerator, and Z is the denominator.
 *
 * @param string $value
 *   The mixed number string to be converted. (in the format "X-Y/Z" where X).
 *
 * @param mixed $whole_number_separator
 *   An optional string or array argument for the separator(s) that are
 *   expected to separate whole number and fractional parts in a mixed
 *   fraction string. The default is to interpret both a hyphen ('-') and
 *   a space (' ') as a separator.
 *
 * @param mixed $fraction_separator
 *   An optional string or array argument for the separator(s) that are
 *   expected to separate the numerator and denominator in a mixed
 *   fraction string. The default is a forward slash ('/').
 *
 * @return MixedFraction
 *   Returns a new mixed fraction object.
 */
function mixed_fraction_from_string($value, $whole_number_separator = NULL, $fraction_separator = NULL) {
  return new MixedFraction($value, NULL, NULL, $whole_number_separator, $fraction_separator);
}